# README #

### What is this repository for? ###

This is the documentation repository for the flatland 2d simulator for ROS.

### Where are the docs? ###

Please visit [flatland.readthedocs.org](https://flatland.readthedocs.org) for the build docs.

### How do I get set up to build locally? ###

* `sudo pip install sphinx sphinx-autobuild`
* check out this repository
* cd docs
* make html
